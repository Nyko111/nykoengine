12/27/14

*Changed assetloader to create sprites dynamically using texture atlas calls and a Map


1/1/15

*Changed confirm pop up buttons to position based on the popup background

*Changed the boolean to show confirmpopup to be internal and removed gameworld reference inside of confirm popup

1/5/15

*Edited and updated ConfirmPopup's fonts to display based on the X and Y of the popup itself

*Changed confirmpopup to have a inputprocessor reference to the processor before its clicked and change back to that one once confirmed or denied

1/29/15

*Moved shop menu to be part of the engine

*Moved ShopButton to be part of the engine

1/30/15

*Added basic login menu design, still doesnt do anything only the menu design is there

2/1/15

*Added ability to log into remote server

*Added Registration system that requires unique user names

2/4/15

*Added save data ability, so data (like currency and stats) can be saved server side (yes, this took me two hours to figure out >.>)

2/7/15

*Edited registration code, edited login code slightly, added better login handling server side and client side

2/15/15

*Added registration menu

2/28/15

*Added hitbox class

3/2/15

*Changed sounds to be dynamically loaded

3/7/15

*Changed shopbutton's to use a task system, allowing them to be more versatile (you must create new task classes that inherit buttontask, then write the code for the task you want completed)

5/2/15

*Changed assetloader to load fonts dynamically

5/6/15

*Added ninepatch support and added allNinePatchMap to assetloader

*Added bitmapfonts map to hold fonts, it also runs through and disposes them all on shutdown

5/9/15

*Added SpawnLogic to the game engine files

5/25/15

*Added itemhandler to handle loading items from text document in a name,price format

7/29/15

*Added two new functions to AnimNyko
-isDone() //returns if done
-ResetAnim() //resets to frame one

8/5/15

*Added particle engine 

8/10/15

*Edited particles to be able to tint

9/5/15

*Added screenshake from tap wizard into main game engine

9/13/15

*Updated GameButton and cleaned up some of the code

9/18/15

*Edited itemhandler code to now load values by "name: " and "price: " in text document

9/25/15

*Changed itemhandler to load items from individual text files, also editing the loading
code a bit

9/27/15

*Removed maxParticleDistance from Emitter

10/6/15

*Fixed ScreenShake to return to original position softly

10/7/15

*Added rand to Assetloader

10/10/15

*Assetloader now holds bitmap fonts in a map to be sure all of them are disposed at end

*Assetloader font generator now saves generated fonts to folder, checks if exists before generating

10/16/15

*Changed to libgdx 1.61

*Removed Gdx-tools and just added bitmapfontwriter from it (called NykoFontWriter)

*Fixed new issues due to removal of font.getbounds

//11-4-15\\

*Added spawnpoint hitbox 

*Added code to spawnlogic to check if enemies collide with spawnpoint

//11-17-15\\

*Added fade in and fade out for currentsong/nextsong in audiomanager

//12-21-15\\

*Changed particles to no longer spritebatch.begin/end, moved to emitter to do in bulk

//12-30-15\\

*Changed manabar to be FillableBar and moved into gameengine

//1-7-16\\

*Added screenlayout 

//4-25-16\\

*Fixed it so that if fill bars go beyond their fill scale, it reduces it down to max fill

//5-2-16\\

*Added PlayTimer

//5-18-16\\

*Added actioncomplete() to tutorialpagebase

*Added actioncomplete to tutorialhandler

//5-20-16\\

*Reimplemented BitMap saving to phone after first generation

//7-14-17\\

*Fixed major bug in horizontal fillable bar class

//12-25-17\\

*Moved audio loading to own functions (loadAudio loads it with assetloader, generate creates the files in game)

*Changed way atlas are loaded in, now can be called from within the app using a filehandle string to load with assetmanager

*Now atlas are loaded with loadAtlas, and sprites are created through createspritesfromatlas

*(These changes are in prep for dynamic loading during gameplay/menu's)