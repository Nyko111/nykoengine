package com.nykoengine.Audio;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.nykoengine.Helpers.Assetloader;


public class AudioManager {
	
	//Current song to play
	public Music currentSong;
	
	//Que of songs
	public ArrayList<Music> songQue = new ArrayList<Music>();

	//Volumes and fade volumes
	public static float MusicVolume = 1f, FadeIn = 0f, FadeOut = 1f, SoundVolume = 1f;
	
	boolean bFadeIn = false, bFadeOut = false, bStopSong = false, bLoopSong;
	
	//Array of sounds currently playing
	public Map<String, Sound> currentSounds = new HashMap<String, Sound>();
	
	public AudioManager() {
		
		
	}	
	
	//Start a new song
	public void StartSong(String songName, boolean bLoopSong) {

		bStopSong = false;
		this.bLoopSong = bLoopSong;
		
		//If the song is the same as currently playing, do nothing
		if (currentSong == Assetloader.songsMap.get(songName) && currentSong.isPlaying()){
			
			return;
		
		//if the song is a new song and a song is already playing do this
		} else if (currentSong != null && currentSong.isPlaying()) {
			
			//Add song to cue and set all the values for it
			songQue.add(Assetloader.songsMap.get(songName));
			
			songQue.get(songQue.size() - 1).setLooping(true);
				
			songQue.get(songQue.size() - 1).setVolume(0f);
				
			songQue.get(songQue.size() - 1).play();
			
			//Start fade in and fadeout
			bFadeIn = true;
			
			bFadeOut = true;
			
		} else {
		
			//If no song is playing, start the chosen song
			currentSong = Assetloader.songsMap.get(songName);

			currentSong.setVolume(0f);
			
			currentSong.play();

			//Fade song in
			bFadeIn = true;
		
		}
		
		
	}
	
	public void Update() {
		
		FadeMusic();


		if (currentSong != null && bLoopSong) {

			if (!currentSong.isPlaying()) {

				currentSong.setVolume(0f);

				currentSong.play();

				//Fade song in
				bFadeIn = true;

			}
		}



	}
	
	public void FadeMusic() {
		
		//Fade
		
		//This is run if a song is currently running and a new one has been set
		if (bFadeIn == true && bFadeOut == true) {
			
			//Fade out
			if (FadeOut > 0f && currentSong.isPlaying()) {
			
				FadeOut -= 0.01f;
				
				if (FadeOut >= 0) {
					
					currentSong.setVolume(FadeOut);
					
				} else { 
					
					currentSong.setVolume(0f);
					
					currentSong.stop();
					
				}
			}

			//Fade in
			if (!currentSong.isPlaying()){
				
				FadeIn += 0.01f;
			
				if (FadeIn <= 1f) {
					
					songQue.get(songQue.size() - 1).setVolume(FadeIn);
				
				} else {
					
					songQue.get(songQue.size() - 1).setVolume(MusicVolume);
					
					currentSong = songQue.get(songQue.size() - 1);
							
					songQue.remove(songQue.size() - 1);
					
					FadeIn = 0;
					FadeOut = MusicVolume;
					
					bFadeIn = false;
					bFadeOut = false;
				}
			}
		
		//This is run if no other songs are running and a song just has to be faded in
		} else if (bFadeIn == true) {
		
			
			if (currentSong.getVolume() <= MusicVolume) {
				
				FadeIn += 0.01f;
				
				if (FadeIn <= MusicVolume) {
					currentSong.setVolume(FadeIn);
				
				} else {
				
					currentSong.setVolume(MusicVolume);
					FadeIn = 0f;
					bFadeIn = false;
				
				}	
			
		}
	
	}  else if (bStopSong == true) {
		
		//Fade out
		
		if (currentSong != null) {
			
			if (FadeOut > 0f && currentSong.isPlaying()) {
		
				FadeOut -= 0.01f;
			
				if (FadeOut >= 0) {
				
					currentSong.setVolume(FadeOut);
				
				} else { 
				
					currentSong.setVolume(0f);
				
					FadeOut = MusicVolume;
				
					bStopSong = false;
				
					currentSong.stop();
					
					currentSong = null;
				
				}
			}
		
		}
		
	}
		
	}
	
	public void SetMusicVolume(float MusicVolume) {
		
		this.MusicVolume = MusicVolume;
		
		this.FadeOut = MusicVolume;
		
		if (currentSong != null)
			currentSong.setVolume(MusicVolume);
		
	}
	
	public void StopSong() {
		
		if (currentSong != null && currentSong.isPlaying())
			bStopSong = true;
		
	}
	
	public void dispose() {
		
		if (currentSong != null)
			currentSong.dispose();
		
		for (int i = 0; i < songQue.size(); i++) {
			
			songQue.get(i).dispose();
			
		}
		
		for (int i = 0; i < currentSounds.size(); i++) {
			
			currentSounds.get(i).dispose();
			
		}
		
	}

}
