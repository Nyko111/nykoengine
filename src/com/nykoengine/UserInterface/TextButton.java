package com.nykoengine.UserInterface;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.nykoengine.Helpers.Assetloader;

public class TextButton extends GameButton{

	BitmapFont font;
	
	GlyphLayout glyphLayout;
	
	float fontX, fontY;
	
	String fontstring;
	
	public TextButton(float X, float Y, Sprite texture, Sprite texture2,
			BitmapFont font, String fontstring, Sound sound, Boolean Togglable) {
		super(X, Y, texture, texture2, sound, Togglable);
		
		this.font = font;
		
		this.fontstring = fontstring;
		
		glyphLayout = new GlyphLayout();
		
		glyphLayout.setText(font, fontstring);
		
		fontX = (X + ((texture.getWidth() * Assetloader.gameScaleX) / 2)) - (glyphLayout.width / 2);
		fontY = (Y + (texture.getHeight() * Assetloader.gameScaleY) / 2) + ((glyphLayout.height) / 2);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void draw(Batch batch, float alpha) {
		
		super.draw(batch, alpha);
		
		font.draw(batch, glyphLayout, fontX, fontY);
	}
	
	public void dispose() {
		
		font.dispose();
		
		glyphLayout = null;
		
		super.dispose();
		
	}
	
}
