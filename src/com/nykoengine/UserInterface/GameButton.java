package com.nykoengine.UserInterface;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.nykoengine.Audio.AudioManager;
import com.nykoengine.Helpers.Assetloader;
import com.nykoengine.Tasks.ButtonTask;

public class GameButton extends Actor{

	//Positions
	protected float X = 0, Y = 0, X2 = 0, Y2 = 0;
	
	protected float Tex1Width, Tex1Height, Tex2Width, Tex2Height;
	
	//Original X and Y positions (for resetting)
	protected float OriginalX, OriginalY;
	
	public boolean pushed = false;
	public boolean go = false;
	public boolean ButtonTogglable = false;
	Sprite texture, texture2;
	public boolean Toggle;
	protected Sound sound;
	
	public ButtonTask task;
	
	public GameButton(float X, float Y, Sprite texture, Sprite texture2, final Sound sound, Boolean Togglable) {
		// TODO Auto-generated constructor stub
		this.texture = texture;
		
		if (texture != null) {
			Tex1Width = texture.getWidth() * Assetloader.gameScaleX;
			Tex1Height = texture.getHeight() * Assetloader.gameScaleY;
		}
			
		this.texture2 = texture2;
		
		if (texture2 != null) {
			Tex2Width = texture2.getWidth() * Assetloader.gameScaleX;
			Tex2Height = texture2.getHeight() * Assetloader.gameScaleY;
			
			//Checks to see if the second texture is bigger then first and corrects X coords if neccessary
			if (texture.getWidth() < texture2.getWidth()) {
				X2 = X - (((Tex2Width - Tex1Width) / 2));
			} else
				X2 = X;
		}
		
		this.X = X;
		this.OriginalX = X;
		
		if (texture2 != null) {
			
		}
		
		this.Y = Y;
		this.OriginalY = Y;
		Y2 = this.Y;
		this.ButtonTogglable = Togglable;
		Toggle = false;
		this.sound = sound;
	
	}
	
	//Toggles the button to pressed/not pressed
	public void Toggle() {
	
		if (ButtonTogglable) {
			Toggle = !Toggle;
		}
	}
	
	//This is called and calls all the other functions when the button is pressed
	public void Pushed() {
	       
		pushed = !pushed;
       	Toggle();
		PlaySound(sound);
       	Vibrate();
	}
	
	
	private void PlaySound(Sound sound) {
		if (sound != null && !Assetloader.bMuteOption) {
			sound.play(AudioManager.SoundVolume);
		}
	}
	
	private void Vibrate() {
		if (Assetloader.bVibrateOption) {
				Gdx.input.vibrate(100);
		}
	}
	
	@Override
	public void draw(Batch batch, float alpha) {
		
		if (texture2 != null) {
		
		//Checks if button is pushed, if it is displays pushed texture, if not displays normal
		if (!pushed && !Toggle) {
			
			batch.draw(texture, X, Y, Tex1Width, Tex1Height);
			setBounds(X, Y, Tex1Width, Tex1Height);
		
		} else {
			
				if (texture != null) {
					batch.draw(texture2, X2, Y2, Tex2Width, Tex2Height);
					setBounds(X2, Y2, Tex2Width, Tex2Height);
				}
			
			}
		} else {
			
			if (texture != null ) {
				batch.draw(texture, X, Y, Tex1Width, Tex1Height);
				setBounds(X, Y, Tex1Width, Tex1Height);
			}
		}
		
	}
	
	//Brings up the confirm popup and sets what to confirm from ConfirmWhat enum
	public void Confirm() {
		
	
	}
	
	public Vector2 getMiddlePos() {
		
		return (new Vector2(X + Tex1Width / 2, Y + Tex1Height / 2));
		
	}
	
	@Override
	public float getX() {
		return X;
	}
	
	@Override
	public float getY() {
		return Y;
		
	}
	
	public void setX(float X) {
		this.X = X;
		
		//Checks to see if the second texture is bigger then first and corrects X coords if neccessary
		if (texture.getWidth() < texture2.getWidth()) {
			X2 = X - (((Tex2Width - Tex1Width) / 2));
		} else
			X2 = X;
		
	}
	
	public void setY(float Y) {
		this.Y = Y;
		
		/*if (texture.getHeight() < texture2.getHeight()) {
			Y2 = Y + (((texture2.getHeight() - texture.getHeight()) / 2) * Assetloader.gameScaleY);
		}*/
	}
	
	public void addY(float Y) {
		this.Y += Y;
	}
	
	//Resets button to original position
	public void resetPos() {
		X = OriginalX;
		Y = OriginalY;
	}
	
	public void setTask(ButtonTask task) {
		
		this.task = task;
		
	}
	
	public void dispose() {
		
		texture = null;
		texture2 = null;
		
		if (sound != null)
			sound.dispose();
	}

}
