package com.nykoengine.UserInterface;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.nykoengine.Helpers.Assetloader;

public class ConfirmPopup extends Stage {
	
	public String sAsk, sName;
	
	public boolean bActiveConfirmPopup = false;
	
	//The popup background positions
	private float PopUpX, PopUpY;
	
	//The font positions
	private float FontX, FontY =  PopUpY / 0.65f;//Assetloader.screenHeight / 1.6f;
	private float FontX2, FontY2 = PopUpY / 0.7f; //Assetloader.screenHeight / 1.7f ;
	
	public GameButton confirmDeny;
	
	private InputProcessor previousinput;
	
	GlyphLayout glyphLayout = new GlyphLayout();
	
	GlyphLayout glyphLayout2 = new GlyphLayout();
	
	private BitmapFont ConfirmPopUpFont = Assetloader.CreateFont(10f, "ConfirmPopUpFont", Assetloader.fonts.get("pizzabot"), Color.BLACK);
	
	GameButton Confirm, Deny;

	public ConfirmPopup() {
		// TODO Auto-generated constructor stub
		
		if (Assetloader.allBGMap.get("ConfirmPopup") != null)
			PopUpX = (Assetloader.screenWidth / 2) - ((Assetloader.allBGMap.get("ConfirmPopup")).getWidth() * Assetloader.gameScaleX / 2);
		
		PopUpY = Assetloader.screenHeight / 2.567f;
		
		
		Confirm = new GameButton(PopUpX / 0.6f, PopUpY / 0.94f, Assetloader.allSpritesMap.get("YesButton"), null, Assetloader.sounds.get("buttonclick"), false);
	
		Confirm.addListener(new InputListener(){
			 public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				 	Confirm.Pushed();
				 	
				 	//put overlay here
				 	
	                return true;
	            }
	            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
	            	Confirm.pushed = false;
	            	
	            	Gdx.input.setInputProcessor(previousinput);
	            	
	            	bActiveConfirmPopup = false;
	            	confirmDeny.Confirm();
	            	
	            }
	        });
		
		Deny = new GameButton(PopUpX / 0.17f, PopUpY / 0.94f, Assetloader.allSpritesMap.get("NoButton"), null, Assetloader.sounds.get("buttonclick"), false);
		
		Deny.addListener(new InputListener(){
			 public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				 	Confirm.Pushed();
				 	
				 	//put overlay here
				 	
	                return true;
	            }
	            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
	            	Deny.pushed = false;
	            	
	            	bActiveConfirmPopup = false;
	            	Deny();
	            	
	            }
	        });
		
		this.addActor(Confirm);
		this.addActor(Deny);
	}
	
	public void setStrings(String sAsk, String sName) {
		
		this.sAsk = sAsk;
		this.sName = sName;
		
		glyphLayout.setText(Assetloader.CreateFont(10f, "ConfirmPopUpFont", Assetloader.fonts.get("pizzabot"), Color.BLACK), sAsk);
		glyphLayout2.setText(Assetloader.CreateFont(10f, "ConfirmPopUpFont", Assetloader.fonts.get("pizzabot"), Color.BLACK), sName);
		
		
		FontX = (Gdx.graphics.getWidth() / 2f) - (glyphLayout.width / 2);
		FontX2 = (Gdx.graphics.getWidth() / 2f) - (glyphLayout2.width / 2);
	}
	
	public void draw(Batch batch) {
		
		batch.begin();
		//Draw the background of the popup
		batch.draw(Assetloader.allBGMap.get("ConfirmPopup"), PopUpX, PopUpY, Assetloader.allBGMap.get("ConfirmPopup").getWidth() * Assetloader.gameScaleX, Assetloader.allBGMap.get("ConfirmPopup").getHeight() * Assetloader.gameScaleY);
		//Draw the asking text
		if (sAsk != null)
			ConfirmPopUpFont.draw(batch, sAsk, FontX, FontY);
		//Draw the lower asking text
		if (sName != null)
			ConfirmPopUpFont.draw(batch, sName, FontX2, FontY2);
		batch.end();
		
		super.draw();
	}
	
	//Show the confirm menu
	public void ShowConfirmMenu() {
		previousinput = Gdx.input.getInputProcessor();
		
		Gdx.input.setInputProcessor(this);
		
		bActiveConfirmPopup = true;	
	}
	
	//Set what button to confirm or deny
	public void setConfirmDeny(GameButton confirmDeny) {
		
		this.confirmDeny = confirmDeny;
		
	}
	
	public void Deny() {
		
		Gdx.input.setInputProcessor(previousinput);
	
	}
	
	@Override
	public void dispose() {
		
		ConfirmPopUpFont.dispose();
		
		glyphLayout = null;
		glyphLayout2 = null;
		
		Confirm.dispose();
		Deny.dispose();
		
		super.dispose();
		
	}
	
}
