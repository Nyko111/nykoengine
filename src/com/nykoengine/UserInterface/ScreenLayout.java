package com.nykoengine.UserInterface;

import com.nykoengine.Helpers.Assetloader;

public class ScreenLayout {
	
	public static float XPadding, YPadding;
	
	public ScreenLayout() {
		
		XPadding = Assetloader.screenWidth / 20f;
		
		YPadding = Assetloader.screenHeight / 32f;
		
	}
	
}
