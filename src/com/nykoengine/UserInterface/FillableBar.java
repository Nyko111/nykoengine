package com.nykoengine.UserInterface;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.nykoengine.Helpers.Assetloader;

public class FillableBar {
	
	//The value (health, xp, mana etc)
	public float Value, maxValue;
	
	//The max fill scaling value
	public float maxfillScale;
	
	//the placement floats 
	public float YTop, YMid, YBottom, XTop, XMid, XBottom;
	
	public float XFill, YFill, XEndAlpha;
	
	//The bar scale (how big the actual bar is) and the fill scale
	public float BarWallWidth = 0;
	
	public float barScale, fillScaleX, fillScaleY;

	Sprite topSprite, topAlpha, midSprite, midAlpha, bottomSprite, bottomAlpha;
	
	Sprite fillSprite;
	
	public float BarFillStartOffset = 0;
	
	public float X, Y;
	
	//What type of bar it is, this changes the algorithm it uses for placement/fill scaling
	public enum BarType {
		VERTICALBAR, HORIZONTALBAR
	}
	
	public BarType barType;
	
	public FillableBar(float X, float Y, float maxValue, float barScale, Sprite endSprite, Sprite midSprite, Sprite topEndSprite, Sprite endSpriteAlpha, Sprite midSpriteAlpha, Sprite fillSprite, BarType barType) {
		
		this.X = X;
		
		this.Y = Y;
			
		this.topAlpha = endSpriteAlpha;
		
		this.midSprite = midSprite;
		
		this.midAlpha = midSpriteAlpha;
		
		this.bottomSprite = endSprite;
		
		if (topEndSprite == null)
			this.topSprite = endSprite;
		else 
			this.topSprite = topEndSprite;
		
		this.bottomAlpha = endSpriteAlpha;
		
		this.fillSprite = fillSprite;
		
		this.barType = barType;
		
		this.barScale = barScale;
		
		this.maxValue = maxValue;
		
		this.Value = 0;
		
		XBottom = X; //Assetloader.screenWidth / 1.175f;
		
		YBottom = Y; //Assetloader.screenHeight / 4f;
		
		YFill = Y;
		
		if (barType.equals(barType.VERTICALBAR)) {
			YMid = YBottom + (bottomSprite.getHeight() * Assetloader.gameScaleY);
		
			YTop = YMid + ((midSprite.getHeight() * Assetloader.gameScaleY) * barScale);
			
			XMid = X;
			
			XTop = X;
			
		} else if (barType.equals(barType.HORIZONTALBAR)) {
			
			YMid = Y;
			
			YTop = Y;
			
			YFill = Y;
			
			XMid = X + (bottomSprite.getWidth() * Assetloader.gameScaleX);
			
			XTop = XMid + ((midSprite.getWidth() * Assetloader.gameScaleX) * barScale);
			
		}
		
		if (barType.equals(barType.HORIZONTALBAR))
			maxfillScale = (((topSprite.getWidth() * Assetloader.gameScaleX) + ((midSprite.getWidth() * Assetloader.gameScaleX) * barScale)) + (bottomSprite.getWidth() * Assetloader.gameScaleX));

		else if (barType.equals(barType.VERTICALBAR)) 
			maxfillScale = (((topSprite.getHeight() * Assetloader.gameScaleY) + ((midSprite.getHeight() * Assetloader.gameScaleY) * barScale) + (bottomSprite.getHeight()) * Assetloader.gameScaleY));
			
		fillScaleY = maxfillScale;
		
		fillScaleX = (fillSprite.getWidth() * Assetloader.gameScaleX);
	}
	
	public void setWallWidth(float BarWallWidth) {
		
		this.BarWallWidth = BarWallWidth;
		
	}
	
	public void draw(Batch batch) {
		
		batch.begin();
		
		if (topAlpha != null) {
			batch.draw(topAlpha, XTop, YTop, topAlpha.getWidth() / 2 * Assetloader.gameScaleX, topAlpha.getHeight() / 2* Assetloader.gameScaleY, topAlpha.getWidth() * Assetloader.gameScaleX, topAlpha.getHeight() * Assetloader.gameScaleY, 1, 1, 180);
		}
		
		if (bottomAlpha != null)
			batch.draw(bottomAlpha, XBottom, YBottom, bottomAlpha.getWidth() * Assetloader.gameScaleX, bottomAlpha.getHeight() * Assetloader.gameScaleY);
		
		
		if (barType.equals(barType.VERTICALBAR)) {


			if (midAlpha != null)
				batch.draw(midAlpha, XMid, YMid, midAlpha.getWidth() * Assetloader.gameScaleX, (midAlpha.getHeight() * Assetloader.gameScaleY) * barScale);


			batch.draw(fillSprite, XBottom + BarWallWidth, YFill + BarFillStartOffset, fillScaleX, fillScaleY);

			batch.draw(midSprite, XMid, YMid, midSprite.getWidth() * Assetloader.gameScaleX, (midSprite.getHeight() * Assetloader.gameScaleY) * barScale);

		}
		else if (barType.equals(barType.HORIZONTALBAR)) {

			if (midAlpha != null)
				batch.draw(midAlpha, XMid, YMid, (midAlpha.getWidth() * Assetloader.gameScaleX)  * barScale, midAlpha.getHeight() * Assetloader.gameScaleY);

			batch.draw(fillSprite, XBottom + BarFillStartOffset, YFill + BarWallWidth, fillScaleY, fillSprite.getHeight() * Assetloader.gameScaleY);

			batch.draw(midSprite, XMid, YMid, (midSprite.getWidth() * Assetloader.gameScaleX) * barScale, midSprite.getHeight() * Assetloader.gameScaleY);

		}
		
		//batch.draw(topSprite, XTop, YTop, (topSprite.getWidth() / 2) * Assetloader.gameScaleX, (topSprite.getHeight() / 2) * Assetloader.gameScaleY, topSprite.getWidth() * Assetloader.gameScaleX, topSprite.getHeight() * Assetloader.gameScaleY, 1, 1, 180);
		
		batch.draw(topSprite, XTop, YTop, topSprite.getWidth() * Assetloader.gameScaleX, (topSprite.getHeight()) * Assetloader.gameScaleY);

		batch.draw(bottomSprite, XBottom, YBottom, bottomSprite.getWidth() * Assetloader.gameScaleX, bottomSprite.getHeight() * Assetloader.gameScaleY);
		
		batch.end();
	}
	
	public void update(float deltaTime) {
		
		fillScaleY = maxfillScale * (Value / maxValue);
		
		if (fillScaleY > maxfillScale) {
			
			fillScaleY = maxfillScale;
			
			Value = maxValue;
			
		} else if (Value < 0) {
			
			fillScaleY = 0;
			
			Value = 0;
			
		}
		
	}
	
	public void FillCompletely() {
		
		Value = maxValue;
		
	}

	public void EmptyCompletely() {

		Value = 0;

	}



	
	public void IncrementFill(float value) {
		
		if (Value <= maxValue)
			Value += value;
		else if (Value > maxValue) {
			Value = maxValue;
		}
		
	}
	
	public void setBarFillOffset(float BarFillStartOffset) {
		
		this.BarFillStartOffset = BarFillStartOffset;
		
		if (barType.equals(barType.HORIZONTALBAR))
			maxfillScale = (((topSprite.getWidth() /* - (BarFillStartOffset * 2)*/ * Assetloader.gameScaleX) + ((midSprite.getWidth() * Assetloader.gameScaleX) * barScale)) + (bottomSprite.getWidth() * Assetloader.gameScaleX)) - (BarFillStartOffset * 2);

		else if (barType.equals(barType.VERTICALBAR)) 
			maxfillScale = (((topSprite.getHeight() * Assetloader.gameScaleY) + ((midSprite.getHeight() * Assetloader.gameScaleY) * barScale) + (bottomSprite.getHeight()) * Assetloader.gameScaleY)) - (BarFillStartOffset * 2);
		
	}
	
	public void AddFill(float value) {
		
		if (Value + value <= maxValue)
			Value += value;
		else
			Value = maxValue;
		
	}
	
	public void SetBarScale(float barScale) {
		
		this.barScale = barScale;
		
		if (barType.equals(barType.VERTICALBAR)) {
			YMid = YBottom + (bottomSprite.getHeight() * Assetloader.gameScaleY);
		
			YTop = YMid + (midSprite.getHeight() * Assetloader.gameScaleY) * barScale;
			
			XMid = X;
			
			XTop = X;
			
		} else if (barType.equals(barType.HORIZONTALBAR)) {
			
			YMid = Y;
			
			YTop = Y;
			
			YFill = Y;
			
			XMid = X + (bottomSprite.getWidth() * Assetloader.gameScaleX);
			
			XTop = XMid + (midSprite.getWidth() * Assetloader.gameScaleX) * barScale;
			
		}
		
		if (barType.equals(barType.HORIZONTALBAR))
			maxfillScale = (((topSprite.getWidth() * Assetloader.gameScaleX) + ((midSprite.getWidth() * Assetloader.gameScaleX) * barScale)) + (bottomSprite.getWidth() * Assetloader.gameScaleX));

		else if (barType.equals(barType.VERTICALBAR)) 
			maxfillScale = (((topSprite.getHeight() * Assetloader.gameScaleY) + ((midSprite.getHeight() * Assetloader.gameScaleY) * barScale) + (bottomSprite.getHeight()) * Assetloader.gameScaleY));
			
		fillScaleY = maxfillScale;
		
		fillScaleX = (fillSprite.getWidth() * Assetloader.gameScaleX);
	}
	
	public void Reset() {
		
		Value = maxValue;
		
	}
	
	public void dispose() {
		
		topSprite = null;
		topAlpha = null;
		midSprite = null;
		midAlpha = null;
		bottomSprite = null;
		bottomAlpha = null;
		
		fillSprite = null;
		
	}

}
