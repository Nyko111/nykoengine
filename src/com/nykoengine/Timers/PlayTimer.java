package com.nykoengine.Timers;

public class PlayTimer {
	
	//Current time
	public static long time;
	public static long seconds;
	public static long minutes;
	//public long minutesSinceLastPlayed;

	public PlayTimer() {
	
		//Initialize time
		time = System.nanoTime();
		seconds = time / 1000000000;
		minutes = seconds / 60;
		
	}
	
	public void Update() {
		
		//Update time
		time = System.nanoTime();
		seconds = time / 1000000000;
		minutes = seconds / 60;

	}
	
	public long TimeSinceLastPlayed(Long LastPlayed) {
		
		System.out.println("Last played:" + new Long(minutes - LastPlayed).toString());
		
		return (minutes - LastPlayed);
		
	}
	
	
	

}
