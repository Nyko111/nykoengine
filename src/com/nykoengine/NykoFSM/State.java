package com.nykoengine.NykoFSM;


/**
 * Created by MacUser on 1/9/17.
 */
public interface State<Object> {

    public void enter(Object object);

    public void exit(Object object);

    public void Update(Object object);




}
