package com.nykoengine.NykoFSM;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by MacUser on 1/9/17.
 */
public class FSM<Object> {

    public static Map<String, State> StateMap = new HashMap<String, State>();

    public static State CurrentState;

    public static State PreviousState;

    Object world;

    public FSM(Object world) {

        this.world = world;

    }

    public void Update(Object object) {

        CurrentState.Update(object);

    }

    public void changeState(String stateName) {

        if (CurrentState != null) {
            PreviousState = CurrentState;

            CurrentState.exit(world);

        }

        CurrentState = StateMap.get(stateName);

        CurrentState.enter(world);

    }


    public void AddState(String stateName, State state) {

        StateMap.put(stateName, state);

    }

    public State getCurrentState() {

        return CurrentState;

    }

    public void previousState() {

        CurrentState.exit(world);

        CurrentState = PreviousState;

        CurrentState.enter(world);

    }

}
