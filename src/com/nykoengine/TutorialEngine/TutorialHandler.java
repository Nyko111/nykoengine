package com.nykoengine.TutorialEngine;


public class TutorialHandler {

	public int CurrentTutorialPage = 0;
	
	public TutorialPageBase[] TutorialPages;
	
	public static Boolean bCompleted = true;
	
	public TutorialHandler() {
		
	}
	
	public void NextTutorial() {
		
		if (CurrentTutorialPage < (TutorialPages.length - 1)) {
			CurrentTutorialPage += 1;
			
		} 
		
	}
	
	public void draw() {
		
		if (TutorialPages != null && TutorialPages[CurrentTutorialPage] != null)
			TutorialPages[CurrentTutorialPage].draw();
		
	}
	
	public void Update() {
		
		TutorialPages[CurrentTutorialPage].Update();
		
	}
	
	public void SetTutorial(TutorialPageBase[] TutorialPages) {
		
		this.TutorialPages = TutorialPages;
		
		CurrentTutorialPage = 0;
		
		bCompleted = false;
		
	}
	
	public static void ActionComplete() {
		
		bCompleted = true;
		
		
	}
	
	public void dispose() {
		
		/*if (TutorialPages != null) {
			for (int i = 0; i < TutorialPages.length; i++) {
			
				TutorialPages[i] = null;
			
			}
		}*/
	}

}
