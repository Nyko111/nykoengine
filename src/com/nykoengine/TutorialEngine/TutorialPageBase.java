package com.nykoengine.TutorialEngine;


public class TutorialPageBase {
	
	//This is a base for tutorial pages
	public TutorialPageBase()  {
		
		
		
	}
	
	public void draw() {
		
		
	}
	
	public void Update() {
		
	}
	
	public void ActionComplete() {
		
		TutorialHandler.ActionComplete();
		
	}

}
