package com.nykoengine.Detection;

import com.badlogic.gdx.math.Vector2;

public class SpawnPoint {
	
	//Position of the spawn point
	public Vector2 Position = new Vector2();
	
	//If the spawnpoint is available to spawn 
	public Boolean bCanSpawn = true;
	
	//Hit box for spawn point (to check if anything is colliding with it resulting in bcanSpawn false)
	public HitBox hitbox;
	
	public SpawnPoint(float SpawnX, float SpawnY, float Width, float Height) {
		
		Position = new Vector2();
		
		hitbox = new HitBox(SpawnX, SpawnY, Width, Height);
		
		Position.x = SpawnX;
		Position.y = SpawnY;
		
	}

}
