package com.nykoengine.Detection;

import com.badlogic.gdx.math.Rectangle;

public class HitBox {

	Rectangle bounds;
	
	public HitBox(float X, float Y, float Width, float Height) {
		
		bounds = new Rectangle(X, Y, Width, Height);
		
		
	}
	
	public void Update(float X, float Y) {
		
		bounds.x = X;
		
		bounds.y = Y;
		
	}
	
	public Rectangle getBounds() {
		
		return bounds;
		
	}
	
	public void setBounds(float Width, float Height) {
		
		this.bounds.width = Width;
		
		this.bounds.height = Height;
		
	}
	
}
