package com.nykoengine.Helpers;

import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetLoaderParameters;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.AsynchronousAssetLoader;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreetypeFontLoader;
import com.badlogic.gdx.utils.Array;

/**
 * Created by MacUser on 1/17/18.
 */
public class FontLoader extends AsynchronousAssetLoader<BitmapFont, FreetypeFontLoader.FreeTypeFontLoaderParameter> {

    public FontLoader(FileHandleResolver resolver) {
        super(resolver);
    }

    @Override
    public Array<AssetDescriptor> getDependencies(String fileName, FileHandle file, FreetypeFontLoader.FreeTypeFontLoaderParameter parameter) {
        return null;
    }

    @Override
    public void loadAsync(AssetManager manager, String fileName, FileHandle file, FreetypeFontLoader.FreeTypeFontLoaderParameter parameter) {

    }

    @Override
    public BitmapFont loadSync(AssetManager manager, String fileName, FileHandle file, FreetypeFontLoader.FreeTypeFontLoaderParameter parameter) {

        FreeTypeFontGenerator fontGenerator = manager.get(parameter.fontFileName + ".gen", FreeTypeFontGenerator.class);

        BitmapFont font = fontGenerator.generateFont(parameter.fontParameters);

        fontGenerator.scaleForPixelHeight(parameter.fontParameters.size);

        return font;
    }


}
