package com.nykoengine.Helpers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.nykoengine.Detection.SpawnPoint;


public class SpawnLogic {
	
	//Cantains the information for what spawns
	public SpawnInfo spawnList;
	
	//Sets if spawnlogic is currently running
	public Boolean bSpawnable = true;
	
	public SpawnPoint[] spawnPoints;
	
	//List to sort for random values between range
	List<Integer> randPosList;
	
	//Holds which number spawn points to spawn at
	public Integer[] spawnArray;
	
	//How many enemy types there are
	public int EnemyTypes = 0;
	
	ScheduledThreadPoolExecutor spawnTimer;

	public SpawnLogic(SpawnPoint[] spawnPoints, SpawnInfo spawnList, int EnemyTypes) {
		
		this.spawnList = spawnList;
		
		this.EnemyTypes = EnemyTypes;
		
		//Set up different unique random number generator
		randPosList = new ArrayList<Integer>();
	   
		for(int i = 0; i < spawnPoints.length; i++){
	        randPosList.add(i);
	    }
		
		this.spawnPoints = spawnPoints;
		
		//Spawn();
		
	}
	
	public void Start() {
		if (spawnTimer == null) {
			
			spawnTimer = new ScheduledThreadPoolExecutor(4);
		
			spawnTimer.scheduleAtFixedRate( new Runnable() {
				@Override
				public void run() {
				
					//Check if done spawning
				
					Spawn(); 
					
				
				}
			}, 1000, 1000, TimeUnit.MILLISECONDS); 
		}
	}
	
	public void Stop() {
		
		spawnTimer.shutdown();
		
		spawnTimer = null;
		
	}
	
	public void Spawn() {
		
		//Shuffle randposlist to select which spawnpoints to spawn on
		Collections.shuffle(randPosList);
		
		//Holds which spawn points to spawn at based on first ones in the array
		spawnArray =  randPosList.subList(0, Assetloader.rand.nextInt(3) + 1).toArray(new Integer[spawnPoints.length]);
		
		for (int i = 0; spawnArray.length > i; i++) {
			
			//Chooses which enemy to spawn
			int chosenEnemy = Assetloader.rand.nextInt(EnemyTypes);
			
			if (spawnArray[i] != null && spawnPoints[i].bCanSpawn && this.bSpawnable) {
				
				
				spawnList.Spawn(chosenEnemy, 10, spawnPoints[i].Position.x, spawnPoints[i].Position.y);
				//Removes one of the spawnable bombs from the pool
					
				//Sets bomb spawned so no more can spawn until it despawns
				
				 
			}
		
		}
		
		if (spawnList.MapEmpty()){
				
				Spawn();
				
		}
		
		//Set bomb spawnable again, after everythings been spawned
		
	}
	
	public void SpawnPointCollision(Rectangle collisioncheck) {
		
		
		for (int i = 0; i < spawnPoints.length; i++) {
			
			if (collisioncheck != null && Intersector.overlaps(collisioncheck, spawnPoints[i].hitbox.getBounds())) {
				
				spawnPoints[i].bCanSpawn = false;
				
			} else {
				
				spawnPoints[i].bCanSpawn = true;
				
			}
			
		}
	}
	
	public void dispose() {
		
		//Stop();
		
	}
	
	//returns true when there is nothing else to spawn
	
	
}
