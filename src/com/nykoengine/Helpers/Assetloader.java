package com.nykoengine.Helpers;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.PixmapPacker;
import com.badlogic.gdx.graphics.g2d.PixmapPacker.Page;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeBitmapFontData;
import com.badlogic.gdx.graphics.g2d.freetype.FreetypeFontLoader;
import com.badlogic.gdx.graphics.glutils.PixmapTextureData;
import com.badlogic.gdx.utils.Array;
import com.nykoengine.Writers.NykoFontWriter;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
//import com.nyko111.SplodyCorn.GoogleInterface;


public class Assetloader {
	
	public static Boolean debug = false;

	public static Boolean bForceRegenerateFonts = false;
	
	public static Random rand;
	
	public static FileHandle[] soundLoader, songLoader, fontLoader;
	
	public static boolean bIsLoaded = false;
	
	public static boolean bMuteOption = false, bVibrateOption = false;
	
	public static AssetManager manager; // = new AssetManager();
	 //Buttons
	
	//Buy buttons
	
	//load font generator

	public static Map<String, TextureAtlas> allAtlasMap = new HashMap<String, TextureAtlas>();
	
	//EHookah Textures
	public static Map<String, Sprite> allSpritesMap = new HashMap<String, Sprite>();
	
	public static Map<String, Sprite> allBGMap = new HashMap<String, Sprite>();
	
	//public static Map<String, NinePatch> allNinePatchMap = new HashMap<String, NinePatch>();
	
	//Sounds
	public static Map<String, Sound> sounds = new HashMap<String, Sound>();
	
	public static Map<String, Music> songsMap = new HashMap<String, Music>();
	
	public static Map<String, FileHandle> fonts = new HashMap<String, FileHandle>();
	
	//Use this to add fonts into so they are properly disposed later
	public static Map<String, BitmapFont> bitmapfonts = new HashMap<String, BitmapFont>();
	
	public final static float screenWidth = Gdx.graphics.getWidth();
	public final static float screenHeight = Gdx.graphics.getHeight();     
    public static float gameWidth;
    public static float gameHeight;
    public static float gameScaleY;
    public static float gameScaleX;
    public static float gameWidthol;
    public static float gameHeightol;
    public static float gameScaleYol;
    public static float gameScaleXol;
	
	public Assetloader(AssetManager manager) {
		
		rand = new Random(System.currentTimeMillis());
		
		if (screenHeight > 1280) {
	    	gameWidth = 1600;
	 	   	gameHeight = 2560;
	 	   	
	    } else {
	    	gameWidth = 800;
	    	gameHeight = 1280;
	    
	    }
		
	    gameScaleY = screenHeight / gameHeight;
	    gameScaleX = screenWidth / gameWidth;
        gameScaleYol = screenHeight / gameHeightol;
        gameScaleXol = screenWidth / gameWidthol;
        
        if (Gdx.app.getType() == ApplicationType.Android) {
			soundLoader = Gdx.files.internal("data/SoundFX").list();
		} else {
			if (debug)
				soundLoader = Gdx.files.internal("./bin/data/SoundFX").list();
			else
				soundLoader = Gdx.files.internal("data/SoundFX").list();
		}
        
        if (Gdx.app.getType() == ApplicationType.Android) {
			songLoader = Gdx.files.internal("data/Music").list();
		} else {
			if (debug)
				songLoader = Gdx.files.internal("./bin/data/Music").list();
			else 
				songLoader = Gdx.files.internal("data/Music").list();
		}
        
        if (Gdx.app.getType() == ApplicationType.Android) {
			fontLoader = Gdx.files.internal("data/Fonts").list();
		} else {
			
			if (debug)
				fontLoader = Gdx.files.internal("./bin/data/Fonts").list();
			else 
				fontLoader = Gdx.files.internal("data/Fonts").list();
			
		}
        
        //This allows assetmanager to properly be destroyed and reininitiated every time
		this.manager = manager;
		
		//manager.get("data/TextDocuments/text.txt", null);
		
		//Font crap
		
		for (FileHandle font : fontLoader) {

			fonts.put(font.nameWithoutExtension(), font);

		}

		//ConfirmPopUpFont = CreateFont(screenHeight / 27.5f, fonts.get("pizzabot"), Color.BLACK);
		
		//ItemButtonNameFont = CreateFont(screenHeight / 23.27f, fonts.get("pizzabot"), Color.BLACK);
		
		//ItemButtonDescripFont = CreateFont(screenHeight / 37.64f, fonts.get("pizzabot"), Color.BLACK);
		
		//CurrencyFont = CreateFont(screenHeight / 20f, fonts.get("pizzabot"), Color.BLACK);
		
		//fontgenerator = new FreeTypeFontGenerator(Gdx.files.internal("data/Fonts/pizzabot.ttf"));

		
	}

	public static void loadAudio() {

		//Sounds / Music

		for (FileHandle sound : soundLoader) {

			manager.load(sound.path(), Sound.class);


		}

		for (FileHandle song : songLoader) {

			manager.load(song.path(), Music.class);

		}



	}

	public static void generateAudio() {

		//Sounds / Music
		for (FileHandle sound : soundLoader) {

			sounds.put(sound.nameWithoutExtension(), manager.get(sound.path(), Sound.class));

			System.out.println("Sound: " + sound.nameWithoutExtension());
		}

		for (FileHandle song : songLoader) {

			songsMap.put(song.nameWithoutExtension(), manager.get(song.path(), Music.class));

			System.out.println("Song: " + song.nameWithoutExtension());

		}

	}

	public static void loadTextureAtlas(String atlaslocation) {

		if (Gdx.files.internal(atlaslocation + ".atlas").exists()) {

			manager.load(atlaslocation + ".atlas", TextureAtlas.class);
			//manager.load(atlaslocation + ".png", Texture.class);
		}


	}

	public static void createSpritesFromAtlas(String name, String atlaslocation) {

		//Atlas
		if (Gdx.files.internal(atlaslocation + ".atlas").exists()) {
			TextureAtlas atlas = manager.get(atlaslocation + ".atlas", TextureAtlas.class);

			allAtlasMap.put(name, atlas);


			for (int i = 0; i < atlas.getRegions().size; i++) {

				Sprite sprite = atlas.createSprite(atlas.getRegions().get(i).name);

				sprite.setOriginCenter();

				allSpritesMap.put(atlas.getRegions().get(i).name, sprite);

				System.out.println("Region name: " + atlas.getRegions().get(i).name);

			}

		}

	}

	public static void createSpritesFromAtlasBG(String name, String atlaslocation) {

		//Atlas
		if (Gdx.files.internal(atlaslocation + ".atlas").exists()) {
			TextureAtlas atlas = manager.get(atlaslocation + ".atlas", TextureAtlas.class);

			allAtlasMap.put(name, atlas);


			for (int i = 0; i < atlas.getRegions().size; i++) {

				Sprite sprite = atlas.createSprite(atlas.getRegions().get(i).name);

				sprite.setOriginCenter();

				allBGMap.put(atlas.getRegions().get(i).name, sprite);

				System.out.println("Region name: " + atlas.getRegions().get(i).name);

			}

		}

	}
	
	public static void loadLargeAssets() {
		
		manager.load("data/Large Atlas/MainLarge.atlas", TextureAtlas.class);
		manager.load("data/Large Atlas/MainLarge.png", Texture.class);
		
		if (Gdx.files.internal("data/Large Atlas/BG/MainBGLarge.atlas").exists()) {
			manager.load("data/Large Atlas/BG/MainBGLarge.atlas", TextureAtlas.class);
			manager.load("data/Large Atlas/BG/MainBGLarge.png", Texture.class);
		}


		//manager.load("data/SoundFX/buttonclick.ogg", Sound.class);
		//manager.load("data/SoundFX/EHookahGurgle.ogg", Music.class);

	}
	
	public static void loadSmallAssets() {
		
		manager.load("data/Small Atlas/MainSmall.atlas", TextureAtlas.class);
		manager.load("data/Small Atlas/MainSmall.png", Texture.class);
	
		if (Gdx.files.internal("data/Small Atlas/BG/MainBGSmall.atlas").exists()) {
			manager.load("data/Small Atlas/BG/MainBGSmall.atlas", TextureAtlas.class);
			manager.load("data/Small Atlas/BG/MainBGSmall.png", Texture.class);
		}
		
		//Sounds / Music
		

	}

	/*//Call to load large screen sprites, sounds and songs.  Generates Sprites from manager textureatlas
	public static void loadLargeTextures() {
		
		//Atlas
		MainAtlas = manager.get("data/Large Atlas/MainLarge.atlas", TextureAtlas.class);
		
		if (Gdx.files.internal("data/Large Atlas/BG/MainBGLarge.atlas").exists())
			BGAtlas = manager.get("data/Large Atlas/BG/MainBGLarge.atlas", TextureAtlas.class);
		
		if (Gdx.files.internal("data/Large Atlas/Ninepatch/NinepatchLarge.atlas").exists())
			NinepatchAtlas = manager.get("data/Large Atlas/Ninepatch/NinepatchLarge.atlas", TextureAtlas.class);
		
		//DEBUG EXPERIENTAL CODE

		for (int i = 0; i < MainAtlas.getRegions().size; i++ ) {

			Sprite sprite = MainAtlas.createSprite(MainAtlas.getRegions().get(i).name);

			sprite.setOriginCenter();

			allSpritesMap.put(MainAtlas.getRegions().get(i).name, sprite);

			System.out.println("Region name: " + MainAtlas.getRegions().get(i).name);

		}

		if (Gdx.files.internal("data/Large Atlas/BG/MainBGLarge.atlas").exists()) {

			for (int i = 0; i < BGAtlas.getRegions().size; i++) {

				allBGMap.put(BGAtlas.getRegions().get(i).name, BGAtlas.createSprite(BGAtlas.getRegions().get(i).name));

				System.out.println("BG name: " + BGAtlas.getRegions().get(i).name);
			}
		}
		
		/*if (Gdx.files.internal("data/Large Atlas/Ninepatch/NinepatchLarge.atlas").exists()) {
			
			for (int i = 0; i < NinepatchAtlas.getRegions().size; i++) {
			
				allNinePatchMap.put(NinepatchAtlas.getRegions().get(i).name, NinepatchAtlas.createPatch(NinepatchAtlas.getRegions().get(i).name));
			
			}
		}
		
		
		//gurgle = manager.get("data/SoundFX/EHookahGurgle.ogg", Music.class);
		//buttonclick = manager.get("data/SoundFX/buttonclick.ogg", Sound.class);
		
	
		//set boolean of loaded

		System.out.println("Game Loaded");
		bIsLoaded = true;
	}

	//Call to load small screen sprites, sounds and songs.  Generates Sprites from manager textureatlas
	public static void loadSmallTextures() {
		
		MainAtlas = manager.get("data/Small Atlas/MainSmall.atlas", TextureAtlas.class);
		
		if (Gdx.files.internal("data/Small Atlas/BG/MainBGSmall.atlas").exists())
			BGAtlas = manager.get("data/Small Atlas/BG/MainBGSmall.atlas", TextureAtlas.class);

		//DEBUG EXPERIENTAL CODE
		
		for (int i = 0; i < MainAtlas.getRegions().size; i++ ) {
			
			Sprite sprite = MainAtlas.createSprite(MainAtlas.getRegions().get(i).name);
			
			sprite.setOriginCenter();
			
			allSpritesMap.put(MainAtlas.getRegions().get(i).name, sprite);
			System.out.println("Region name: " + MainAtlas.getRegions().get(i).name);
			
			
		}
		
		if (Gdx.files.internal("data/Small Atlas/BG/MainBGSmall.atlas").exists()) {
			for (int i = 0; i < BGAtlas.getRegions().size; i++) {
			
				allBGMap.put(BGAtlas.getRegions().get(i).name, BGAtlas.createSprite(BGAtlas.getRegions().get(i).name));

				System.out.println("BG name: " + BGAtlas.getRegions().get(i).name);
			}
		}
		
		
		if (Gdx.files.internal("data/Small Atlas/Ninepatch/NinepatchSmall.atlas").exists()) {
			
			for (int i = 0; i < NinepatchAtlas.getRegions().size; i++) {
			
				allNinePatchMap.put(NinepatchAtlas.getRegions().get(i).name, NinepatchAtlas.createPatch(NinepatchAtlas.getRegions().get(i).name));
			
			}
		}
		
		//gurgle = manager.get("data/SoundFX/EHookahGurgle.ogg", Music.class);
		//buttonclick = manager.get("data/SoundFX/buttonclick.ogg", Sound.class);
		
		
		//set boolean of loaded
		
		System.out.println("Game Loaded");
		bIsLoaded = true;
	}*/

	public static BitmapFont CreateFont(float size, String name,  FileHandle font, Color color) {

		BitmapFont bitmapFont;

		if ((!Gdx.files.local("/SavedFonts/").exists() || !Gdx.files.local("/SavedFonts/").child(name + ".fnt").exists()) || bForceRegenerateFonts ) {

			System.out.println("Creating font: " + name);

			FreeTypeFontGenerator fontGenerator = new FreeTypeFontGenerator(font);

			FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();

			parameter.size = (int)Math.ceil(size);

			fontGenerator.scaleForPixelHeight(parameter.size);

			//bitmapFont.setScale(bitmapFont.getScaleX() + width, bitmapFont.getScaleY());
			//bitmapFont.getRegion().getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
			parameter.minFilter = Texture.TextureFilter.Nearest;
			parameter.magFilter = Texture.TextureFilter.MipMapLinearNearest;
			//fontGenerator.dispose();

			//bitmapFont = fontGenerator.generateFont(parameter);
			//bitmapFont.setColor(color);

			//////////

			parameter.packer = new PixmapPacker(512, 512, Format.RGBA8888, 2, false);
			parameter.characters = FreeTypeFontGenerator.DEFAULT_CHARS;
			FreeTypeBitmapFontData fontData = fontGenerator.generateData(parameter);

			Array<Page> pages = parameter.packer.getPages();
		    Array<TextureRegion> texRegions = new Array<TextureRegion>();
		    for (int i=0; i<pages.size; i++) {
		        Page p = pages.get(i);
		        Texture tex = new Texture(new PixmapTextureData(p.getPixmap(), p.getPixmap().getFormat(), false, false, true)) {
		            @Override
		            public void dispose () {
		                super.dispose();
		                getTextureData().consumePixmap().dispose();
		            }
		        };
		        tex.setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
		        texRegions.add(new TextureRegion(tex));
		    }



		    bitmapFont = new BitmapFont(fontData, texRegions, false);

		    bitmapFont.setColor(color);

		   // bitmapFont = new BitmapFont();

		    //bitmapFont.getRegions().addAll(texRegions);

		    //bitmapFont = new BitmapFont();
		   // bitmapFont.setColor(color);

			saveFontToFile(bitmapFont, parameter.size, name, parameter.packer);

			//dispose
			fontGenerator.dispose();
			parameter.packer.dispose();

			//PixmapIO.writeCIM(Gdx.files.local(".bin/data/SavedFonts/"), bitmapFont.getRegion().getTexture().getTextureData().consumePixmap());

			//ImageIO.write(bitmapFont.getRegion().getTexture().getTextureData().consumePixmap(), "test", arg2)

			} else {

				System.out.println("Loading font from file: " + name);

				if (debug) {

					bitmapFont = new BitmapFont(Gdx.files.local("/SavedFonts/").child(name + ".fnt"));

				} else {

					//manager.load(Gdx.files.local("/SavedFonts/").child(name + ".fnt").toString(), BitmapFont.class);
					bitmapFont = new BitmapFont(Gdx.files.local("/SavedFonts/").child(name + ".fnt"));

				}

			}

		bitmapfonts.put(name, bitmapFont);

		return bitmapFont;


	}

	private static boolean saveFontToFile(BitmapFont font, int fontSize, String fontName, PixmapPacker packer) {
		FileHandle fontFile = getFontFile(fontName + ".fnt"); // .fnt path
		FileHandle pixmapDir = getFontFile(fontName); // png dir path
		NykoFontWriter.setOutputFormat(NykoFontWriter.OutputFormat.Text);

		String[] pageRefs = NykoFontWriter.writePixmaps(packer.getPages(), pixmapDir, fontName);
		//Tools.log.debug(String.format("Saving font [%s]: fontfile: %s, pixmapDir: %s\n", fontName, fontFile, pixmapDir));
		// here we must add the png dir to the page refs
		for (int i = 0; i < pageRefs.length; i++) {
			pageRefs[i] = fontName + "/" + pageRefs[i];
			//Tools.log.debug("\tpageRef: " + pageRefs[i]);
		}

		System.out.println("Saving font: " + fontName);

		NykoFontWriter.writeFont(font.getData(), pageRefs, fontFile, new NykoFontWriter.FontInfo(fontName, fontSize), 1, 1);
		return true;
	}

	private static FileHandle getFontFile(String filename) {
		return Gdx.files.local("/SavedFonts/" + filename);
	}
	
	public static void dispose() {
		
		if (bIsLoaded) {
			// TODO Auto-generated method stub

			for (int i = 0; i < allAtlasMap.size(); i++) {

				if (allAtlasMap.get(i) != null) {

					allAtlasMap.get(i).dispose();

				}

			}

			allAtlasMap.clear();
			
			for (int i = 0; i < allSpritesMap.size(); i++) {
				
				if (allSpritesMap.get(i) != null) {
					
					allSpritesMap.get(i).getTexture().dispose();
					
				}
				
			}
			
			allSpritesMap.clear();
			
			for (int i = 0; i < allBGMap.size(); i++) {
				
				if (allBGMap.get(i) != null) {
					
					allBGMap.get(i).getTexture().dispose();
					
				}
				
			}
			
			allBGMap.clear();
			
			sounds.clear();
			
			songsMap.clear();
			
			//Asset manager
			manager.dispose();
			
			//Fonts
			
			for (int i = 0; i < bitmapfonts.size(); i++) {
				
				if (bitmapfonts.get(i) != null) {
					
					bitmapfonts.get(i).getRegion().getTexture().getTextureData().consumePixmap().dispose();
					bitmapfonts.get(i).dispose();
					
				}
				
			}
			
			bIsLoaded = false;
		}
		
		
		//ALWAYS LOADED
		
		//MainFont.getRegion().getTexture().getTextureData().consumePixmap().dispose();
		//ConfirmPopUpFont.getRegion().getTexture().getTextureData().consumePixmap().dispose();
		//ItemButtonNameFont.getRegion().getTexture().getTextureData().consumePixmap().dispose();
		//ItemButtonDescripFont.getRegion().getTexture().getTextureData().consumePixmap().dispose();
		//CurrencyFont.getRegion().getTexture().getTextureData().consumePixmap().dispose();
		
	}

}
