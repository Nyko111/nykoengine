package com.nykoengine.Helpers;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.files.FileHandle;

public class ParticleLoader {
	
		//This holds the name of items connected with how many of them you have (the int)
		public Map<String, Integer> ItemList = new HashMap<String, Integer>();
		
		//This is the prices of all of the items
		public List<Integer> allItemPrices = new ArrayList<Integer>(); 
		
		//This holds all thte names of the items
		public List<String> allItemNames = new ArrayList<String>(); 
		
		FileReader txtReader;
		
		FileHandle fileHandle = Gdx.files.internal("data/TextDocuments/test.txt");
	
	public ParticleLoader() {
		
		if (Gdx.app.getType() == ApplicationType.Android) {
			//fileHandle = Gdx.files.internal("data/TextDocuments/test.txt");
		 } else {
			// fileHandle = Gdx.files.internal("./bin/data/TextDocuments/test.txt");
		 }
		
		    String line;
		   
		    try {
		    	BufferedReader in = new BufferedReader(new InputStreamReader(fileHandle.read()));
		    	while((line=in.readLine())!=null){
				    String[] values = line.split(" ");
				    for(String v:values){
				        String[] PriceName = v.split(",");
				        int Price = Integer.parseInt(PriceName[1]);
				        
				        ItemList.put(PriceName[0], 0);
				        
				        allItemPrices.add(Price);
				        allItemNames.add(PriceName[0]);
				        System.out.println(PriceName[0]);
				    }
				}
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
	}

}
