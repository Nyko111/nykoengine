package com.nykoengine.Helpers;

public class NykoTimer
{
    public float remaining;
    protected float interval;

    public NykoTimer(float interval)
    {
        reset(interval);
    }

    public boolean hasTimeElapsed() { return (remaining < 0.0F); }
    
    public void reset() { remaining = interval; }

    public void reset(float interval) {
        this.interval = interval;
        this.remaining = interval;
    }

    public void update(float delta) { remaining -= delta; }
}
