package com.nykoengine.Helpers;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;

public class ItemHandler {
	
	//This holds the name of items connected with how many of them you have (the int)
	public Map<String, Integer> ItemList = new HashMap<String, Integer>();
	
	//This is the prices of all of the items
	public List<Integer> allItemPrices = new ArrayList<Integer>(); 
	
	//This holds all thte names of the items
	public List<String> allItemNames = new ArrayList<String>(); 
	
	FileReader txtReader;
	
	public static FileHandle[] itemLoader;
	
	//public static Map<String, FileHandle> items;
	
	public ItemHandler() {
		
		if (Gdx.app.getType() == ApplicationType.Android) {
			itemLoader = Gdx.files.internal("data/TextDocuments/Items").list();
		} else {
			
			if (Assetloader.debug)
				itemLoader = Gdx.files.internal("./bin/data/TextDocuments/Items").list();
			else 
				itemLoader = Gdx.files.internal("data/TextDocuments/Items").list();
			
		}
		
		    String line;
		   
		    for (FileHandle item : itemLoader) {
		    	try {
		    		BufferedReader in = new BufferedReader(new InputStreamReader(item.read()));
		    		while((line=in.readLine())!=null){
		    			String[] values = line.split("\n");
		    			for(String v:values){
				    	
		    				//String[] PriceName = v.split(",");
		    				
		    				//for (int i = 0; PriceName.length > i; i++) {
				        	
		    					if (v.contains("Name: ")) {
				        		
		    						allItemNames.add(v.substring(6));
				        		 
		    						System.out.println(v.substring(6));
				        		
		    					} else if (v.contains("Price: ")) {
				        		
		    						int Price = Integer.parseInt(v.substring(7));
		    						allItemPrices.add(Price);
				        		
		    						System.out.println(v.substring(7));
				        		
		    					}
				        	
		    				//}
				      
		    			}	
		    		}
		    	
		    	
		    	in.close();
		    	} catch (NumberFormatException e) {
		    		// TODO Auto-generated catch block
		    		e.printStackTrace();
		    	} catch (IOException e) {
		    		// TODO Auto-generated catch block
		    		e.printStackTrace();
		    	}
		    
		    }
		    
		    for (int i = 0; allItemNames.size() > i; i++) {
		    	
		    	ItemList.put(allItemNames.get(i), allItemPrices.get(i));
		    	
		    }
		
	}

}
