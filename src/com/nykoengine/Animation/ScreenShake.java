package com.nykoengine.Animation;

import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.nykoengine.Helpers.Assetloader;

public class ScreenShake {
	
	//How long screen shaking will lsat
	float Duration;
	
	//How intense to shake the screen
	float Intensity;
	
	//Reports whether the screen is done shaking or not
	public boolean bIsDone = false;
	
	//Holds original position, and the velocity of movement
	Vector2 OrigPosition, velocity;
	
	public ScreenShake(float InitX, float InitY, float Duration) {
		
		this.Duration = Duration;
		
		OrigPosition = new Vector2(InitX, InitY).cpy();
		
	}
	
	public void shakeScreen(OrthographicCamera cam, Batch batch) {
		
		//If the screen is not shaking, run code to shake
		if (!bIsDone) {
			
			//If duration is greater then 0
			if (Duration > 0) {
			
				//Move the camera
				cam.translate((Assetloader.rand.nextFloat() - .5f) * 2 * Intensity , (Assetloader.rand.nextFloat() - .5f) * 2 * Intensity);
				
				cam.update();
				
				batch.setProjectionMatrix(cam.combined);
				
				//Reduce duration
				Duration -= Gdx.graphics.getDeltaTime();
				
				
				
			} else {
				
				//If screen shaking is done return screen to original position gracefully
				if (cam.position.x - OrigPosition.x >= 5) {
					
					cam.position.x -= 1f * Intensity;
					
				} else if (cam.position.x - OrigPosition.x <= -5) {
					
					cam.position.x += 1f * Intensity;
					
				} else {
					
					cam.position.x = OrigPosition.x;
					
				}
				
				if (cam.position.y - OrigPosition.y >= 5) {
					
					cam.position.y -= 1f * Intensity;
					
				} else if (cam.position.y - OrigPosition.y <= -5) {
					
					cam.position.y += 1f * Intensity;
					
				} else {
					
					cam.position.y = OrigPosition.y;
					
				}
				
				cam.update();
				
				batch.setProjectionMatrix(cam.combined);
				
				//If camera is in original position, set to done
				if (cam.position.x == OrigPosition.x && cam.position.y == OrigPosition.y)
					bIsDone = true;
			}	
			
		}
		
	}
	
	public void setDuration(float Duration) {
		
		this.Duration = Duration;
	}
	
	public void setIntensity(float Intensity) {
		
		this.Intensity = Intensity;
		
	}
	
	public void Reset(float Duration) {
		
		bIsDone = false;
		
		this.Duration = Duration;
		
		
		
	}

}
