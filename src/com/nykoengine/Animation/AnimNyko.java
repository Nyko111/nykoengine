package com.nykoengine.Animation;

import java.util.ArrayList;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class AnimNyko
{
    //Create a class called frame, it will hold a sprite and the length of time that sprite will be visable
	private class Frame
    {
        Sprite image;
        float length;
    }
	
    ArrayList<Frame> frames;

    public boolean repeat;
    boolean done;
    int index;
    float time;
    
    //The current frame being displayed
    Frame activeFrame;

    public AnimNyko()
    {
        frames = new ArrayList<Frame>();
    }

    public void addFrame(Sprite image, float length)
    {
        Frame f = new Frame();
        f.image = image;
        f.length = length;

        if (frames.size() == 0)
        {
            index = 0;
            activeFrame = f;
            time = length;
        }
        done = false;
        frames.add(f);
    }

    public void update(float delta)
    {
        if (done)
            return;

        time -= delta;
        if (time <= 0)
        { 
           if (index >= (frames.size() - 1))
           {
               if (repeat)
               {
                   index = 0;
               }
               else
               {
                   done = true; 
               }
           }
           else
           {
               index++;
           }
           activeFrame = frames.get(index);
           time = activeFrame.length;
        }
    }

    public void render(SpriteBatch batch)
    {
        batch.draw(activeFrame.image, 50, 50);
    }
    
    public Sprite currentSprite() {
    	return activeFrame.image;
    }
    
    public boolean isDone() {
    	
    	return done;
    	
    }
    
    public void ResetAnim() {
    	
    	index = 0;
    	done = false;
    	
    }
    
    public void dispose() {
    	
    	activeFrame = null;
    	
    	frames.clear();
    	
    	frames = null;
    	
    }
    
}

