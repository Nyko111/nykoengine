package com.nykoengine.ParticleEngine;

import java.util.ArrayList;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.nykoengine.Helpers.Assetloader;

public class Emitter {
	
	//If emitter is done
	boolean isDone = false;
	
	//Particle sprite, and its alpha
	Sprite particleSprite, additiveAlpha;
	
	//Max particles to be created at one time
	int MaxParticles;
	
	//How long the emitter will be alive
	private float EmitterTimer;
	
	private float EmitterTimerOrig;
	
	//How long the particles will live for (pass into rand to get a random)
	private float ParticleTimer;
	
	float gravity;

	//An array of all the particles currently alive
	public ArrayList<Particle> particles;
	
	//Position of the emitter (where it will emit from)
	Vector2 position;
	
	//Vector2 maxParticleDistance;
	
	//Max velocity particles will move and which directions
	//Pass values into rand to get a random range
	Vector2 particleMovement;
	
	ScheduledThreadPoolExecutor addParticleTimer = new ScheduledThreadPoolExecutor(2);
	
	Color tintColor = new Color(Color.WHITE);
	
	boolean XRandDirection = true, YRandDirection = false;
	
	public Emitter(Sprite particleSprite, Sprite additiveAlpha, Vector2 position, Vector2 particleMovement, float gravity, float ParticleTimer, float EmitterTimer, final int MaxParticles, int EmitterDelay) {
		
		this.particleSprite = particleSprite;
		
		this.additiveAlpha = additiveAlpha;
		
		this.position = position;
		
		//this.maxParticleDistance = maxParticleDistance;
		
		this.particleMovement = particleMovement;
		
		this.ParticleTimer = ParticleTimer;
		
		this.EmitterTimer = EmitterTimer;
		
		this.EmitterTimerOrig = EmitterTimer;
		
		this.MaxParticles = MaxParticles;
		
		this.gravity = gravity;
		
		particles = new ArrayList<Particle>();
		
		addParticleTimer.scheduleAtFixedRate( new Runnable() {
			@Override
			public void run() {
				
				if (particles.size() < MaxParticles && !isDone) {
					
					addParticle();
					
				}
				
			}
		}, 0, EmitterDelay, TimeUnit.MILLISECONDS);
		
		
	}
	
	
	public void draw(Batch batch, float Alpha) {
		
		for (int i = 0;particles.size() > i; i++) {
			
			if (particles.get(i) != null)
				particles.get(i).draw(batch, Alpha);
			
		}
		
	}
	
	
	public void update(float deltaTime) {
		
		for (int i = 0;particles.size() > i; i++) {
			
			if (particles.get(i).isDone()) {
				
				particles.remove(i).dispose();
				
			} else {
				
				particles.get(i).update(deltaTime);
				
			}
			
		}
		
		if (EmitterTimer != -1) {
			
			EmitterTimer -= deltaTime;
		
			if (EmitterTimer <= 0 && particles.size() <= 0) {
				isDone = true;
				addParticleTimer.shutdown();
			}
		}
		
	}
	
	public void setRandDirections(boolean XRandDirection, boolean YRandDirection) {
		
		this.XRandDirection = XRandDirection;
		
		this.YRandDirection = YRandDirection;
		
	}
	
	public void addParticle() {
		
		Vector2 tempMovement = new Vector2();
		
		
		
		tempMovement.x = RandomRange(0, (int)particleMovement.x, XRandDirection);
		tempMovement.y = RandomRange((int)particleMovement.y / 2, (int)particleMovement.y, YRandDirection);
		
		
		if (EmitterTimer == -1 || EmitterTimer > 0) {
			
			particles.add(new Particle(particleSprite, additiveAlpha, position.cpy(), tempMovement.cpy(), gravity, 1f, ParticleTimer));
			particles.get(particles.size() - 1).setColor(tintColor);
			
		}
	}
	
	public float RandomRange(int Min, int Max, boolean RandomReverse) {
		
		float randNumber = Assetloader.rand.nextFloat() + Assetloader.rand.nextInt((Max - Min) + 1) + Min;
		
		if (randNumber >= Max) {
			
			randNumber = Max;
			
		}

		if (RandomReverse) {
			
			Boolean Positive = Assetloader.rand.nextBoolean();
			
			if (Positive) {
				return randNumber;
			} else {
				return -randNumber;
			}
		} else {
			return randNumber;
		}
		
		
		
	}
	
	public void setColor(Color tintColor) {
		
		this.tintColor = tintColor;
		
	}
	
	public void setPosition(float X, float Y) {
		
		position.x = X;
		position.y = Y;
		
	}
	
	public void Reset() {
		
		isDone = false;
		
		EmitterTimer = EmitterTimerOrig;
		
	}
	
	public Boolean isDone() {
		
		return isDone;
		
	}
	
	public void dispose() {
		
		if (particleSprite != null)
			particleSprite.getTexture().dispose();
		
		if (additiveAlpha != null)
			additiveAlpha.getTexture().dispose();
		
		for (int i = 0; i < particles.size(); i++) {
			
			if (particles.get(i) != null)
				particles.get(i).dispose();
			
		}
		
	}
	
}
