package com.nykoengine.ParticleEngine;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.nykoengine.Helpers.Assetloader;

public class Particle {
	
	float particleTimer;
	
	float alpha;
	
	Sprite particleSprite, additiveAlpha;
	
	Vector2 position;
	
	float gravity;
	
	Vector2 movement;
	
	//float Speed = 1f;
	
	boolean isDone = false;
	
	Color tintColor = new Color(Color.WHITE);
	
	public Particle(Sprite particleSprite, Sprite additiveAlpha, Vector2 position, Vector2 movement, float gravity, float Alpha, float particleTimer) {
		
		this.particleSprite = particleSprite;
		
		this.additiveAlpha = additiveAlpha;
		
		this.gravity = gravity;
		
		this.position = position;
		
		this.movement = movement;
		
		this.alpha = Alpha;
		
		this.particleTimer = particleTimer;
		
	}
	
	public void draw(Batch batch, float Alpha) {
		
		batch.setColor(tintColor.r, tintColor.g, tintColor.b, alpha);
		
		if (additiveAlpha != null) { 
			batch.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE);
		
			batch.draw(additiveAlpha, position.x - (((additiveAlpha.getWidth() / 2) - (particleSprite.getWidth() / 2)) * Assetloader.gameScaleX), position.y - (((additiveAlpha.getHeight() / 2) - (particleSprite.getHeight() / 2)) * Assetloader.gameScaleY), additiveAlpha.getWidth() * Assetloader.gameScaleX, additiveAlpha.getHeight() * Assetloader.gameScaleY);
		
			batch.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
		}
		
		batch.draw(particleSprite, position.x, position.y, particleSprite.getWidth() * Assetloader.gameScaleX, particleSprite.getHeight() * Assetloader.gameScaleY);
		
		batch.setColor(1.0f, 1.0f, 1.0f, 1f);
		
		
		
	}
	
	public void update(float deltaTime) {
		
		//if (position.y >= maxDistanceY) {
			
			//movement.y = -movement.y;
			
		//}
		
		movement.y -= gravity;
		
	
		if (alpha > 0.05)
			alpha -= deltaTime;
		
		
		//move particle
		position.add((movement.x) * deltaTime, (movement.y) * deltaTime);
		//position.x -= movement.x * deltaTime;
		//position.y -= movement.y * deltaTime;
		
		particleTimer -= deltaTime;
		
		if (particleTimer <= 0) {
			
			isDone = true;
			
		}
		
	}
	
	public void setGravity(float gravity) {
		
		this.gravity = gravity;
		
	}
	
	public void setColor(Color tintColor) {
		
		this.tintColor = tintColor;
		
	}
	
	public boolean isDone() {
		
		return isDone;
		
	}

	public void dispose() {
		
		this.particleSprite = null;
		
		this.additiveAlpha = null;
		
		this.tintColor = null;
		
		this.position = null;
		
		this.movement = null;
		
	}
	
}
