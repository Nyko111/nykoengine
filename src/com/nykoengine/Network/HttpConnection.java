package com.nykoengine.Network;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net;
import com.badlogic.gdx.net.HttpParametersUtils;

import java.util.Map;

/**
 * Created by MacUser on 11/20/16.
 */
public class HttpConnection {

    public static Net.HttpRequest httpPost, httpGet;

    public HttpConnection(String serverURL) {

        httpPost = new Net.HttpRequest(Net.HttpMethods.POST);
        httpPost.setUrl(serverURL);

        httpGet = new Net.HttpRequest(Net.HttpMethods.GET);
        httpGet.setUrl(serverURL);

    }

    public void setPostContent(Map<String, String> contentMap) {

        httpPost.setContent(HttpParametersUtils.convertHttpParameters(contentMap));

    }

    public void HttpPost(Net.HttpResponseListener responseListener) {

        Gdx.net.sendHttpRequest(httpPost, responseListener);

    }

    public void setURL(String serverURL) {

        httpPost.reset();
        httpPost = new Net.HttpRequest(Net.HttpMethods.POST);
        httpPost.setUrl(serverURL);

        httpGet.reset();
        httpGet = new Net.HttpRequest(Net.HttpMethods.GET);
        httpGet.setUrl(serverURL);

    }

}
