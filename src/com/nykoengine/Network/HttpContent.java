package com.nykoengine.Network;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by MacUser on 11/29/16.
 */
public class HttpContent {

    public Map<String, String> postMap;

    public Map<String, String> getMap;

    public HttpContent() {

        postMap = new HashMap<String, String>();

        getMap = new HashMap<String, String>();

    }


    public void AddgetMap(String key, String value) {

        getMap.put(key, value);

    }

    public void AddpostMap(String key, String value) {

        postMap.put(key, value);

    }


}
